package Controller;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import DAO.DAOLogin;
import DAO.DAOStock;
import model.materiels;
import model.MyDefaultModel;
import model.User;
import util.HibernateUtil;
import views.ShowMedic;
import views.login;
import views.stock;

public class MedicController {

	private ShowMedic fenetre;
	private String txt;
	private int id;
	DAOStock daostock;
	materiels produit;
	public MedicController() {
		txt = "hello world";
	}
	User user;
	
	public MedicController(ShowMedic f, DAOStock l, int i, User u) throws SQLException{
		
		this.fenetre = f;
		this.daostock = l;
		this.id = i;
		this.user = u;
		
		init();
		
		f.getBtnModifier().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					doUpdate();
				} catch (HibernateException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		

		f.getBtnRetour().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					doRetour();
				} catch (HibernateException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		
		f.setVisible(true);
		
	}
	
	public void init() {
		produit = daostock.find(id);
		
		fenetre.getTxtNom().setText(produit.getNom());
		fenetre.getTxtQuant().setText(String.valueOf(produit.getQuantite()));
		fenetre.getTxtID().setText(String.valueOf(produit.getId()));
		
		System.out.println(id);
	}
	public void ShowStock() throws HibernateException, SQLException {
		
		new StockController(new stock(), new DAOStock(HibernateUtil.getSessionFactory().openSession()), user);
		
	}
	public void doUpdate() throws HibernateException, SQLException {	
	      
		produit.setNom(String.valueOf(fenetre.getTxtNom().getText()));
		produit.setQuantite(Integer.parseInt(String.valueOf(fenetre.getTxtQuant().getText())));
		fenetre.setVisible(false);
		daostock.saveOrUpdate(produit);
		
		ShowStock();
		
		
	}
	public void doRetour() throws HibernateException, SQLException {	
		
		fenetre.setVisible(false);
	
		ShowStock();		
	}
	
}

