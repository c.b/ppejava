package Controller;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import DAO.DAOLogin;
import DAO.DAOStock;
import model.materiels;
import model.MyDefaultModel;
import model.User;
import util.HibernateUtil;
import views.ShowMedic;
import views.login;
import views.stock;

public class StockController {

	private stock stock_view;
	private String txt;
	DAOStock daostock;
	
	List<materiels> materiels;
	MyDefaultModel model;
	
	User user;
	
	
	public StockController() {
		txt = "hello world";
	}
	
	public StockController(stock f, DAOStock l, User u) throws SQLException{
		
		this.stock_view = f;
		this.daostock = l;
		this.user = u;
		init();
		
		stock_view.getBtnDelete().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {

					doDelete();
			}
		});
		
		stock_view.getBtnUpdate().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					doUpdate();
				} catch (HibernateException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		stock_view.getBtnNew().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
					try {
						doNew();
					} catch (HibernateException | SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			}
		});
		


		stock_view.getBtnSearch().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {

					doSearch();
			}
		});
		f.setVisible(true);
		
	}
	
	public void init() {
		
		materiels = daostock.findAll();
		
		stock_view.getLblNom().setText(user.getNom() + ", vous êtes " + user.getRole());
		
		
		
		if(user.getRole().equals("administrateur")) {
			stock_view.getBtnNew().setEnabled(true);
			stock_view.getBtnDelete().setEnabled(true);
			stock_view.getBtnUpdate().setEnabled(true);
		}

		model = new MyDefaultModel(materiels);
		stock_view.getJTable().setModel(model);
		
		stock_view.setVisible(true);
	}
	
	public void doUpdate() throws HibernateException, SQLException {
		
		stock_view.setVisible(false);

		String selectedrow = model.getValueAt(stock_view.getJTable().getSelectedRow(), 0).toString();
		int row = Integer.parseInt(selectedrow);
		
		
		new MedicController(new ShowMedic(), new DAOStock(HibernateUtil.getSessionFactory().openSession()), row, user);
	
		
	}
	

	public void doNew() throws HibernateException, SQLException {
		
		stock_view.setVisible(false);
				

		new NewMedicController(new ShowMedic(), new DAOStock(HibernateUtil.getSessionFactory().openSession()), user);
		model.fireTableDataChanged();
		
	}
	
	public void doSearch() {
		materiels = daostock.findProductBy(stock_view.getTextSearch().getText());
		System.out.println(materiels);
		
		model = new MyDefaultModel(materiels);
		stock_view.getJTable().setModel(model);
		
	}
	
	public void doDelete() {
		
		int row = stock_view.getJTable().getSelectedRow();
		if(row != -1) {
			materiels c = materiels.get(row);
			daostock.delete(c);
			materiels.remove(row);
			model.fireTableDataChanged();
			
		}else {
				
		}
		

		

		
	}
	
}

