package Controller;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import DAO.DAOLogin;
import DAO.DAOStock;
import model.User;
import util.HibernateUtil;
import views.login;
import views.stock;

public class LoginController {

	private login login_view;
	private String txt;
	DAOLogin daologin;
	
	public LoginController() {
		txt = "hello world";
	}
	
	public LoginController(login f, DAOLogin l) throws SQLException{
		
		this();
		login_view = f;
		this.daologin = l;
		
		f.getBtnLogin().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				doLogin();
			}
		});
		
		f.setVisible(true);
		
	}

	public void doLogin() {
	    JFrame frame = new JFrame("JOptionPane showMessageDialog example");

		
		if(daologin.check_user(login_view.getTxtid().getText(), login_view.getTxtpass().getText()) == null) {
			JOptionPane.showMessageDialog(frame, "Email ou mot de passe incorrect");
		}else {
			LoginOK();
			login_view.dispose();
		}
	}
	
	public void LoginOK() {
		
		try {
			User user = daologin.check_user(login_view.getTxtid().getText(), login_view.getTxtpass().getText());
			
			new StockController(new stock(), new DAOStock(HibernateUtil.getSessionFactory().openSession()), user);		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}

