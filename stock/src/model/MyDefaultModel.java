package model;
import java.util.HashSet;
import java.util.List;

import javax.swing.table.DefaultTableModel;


public class MyDefaultModel extends DefaultTableModel{
	
	private List<materiels> cities ;
	private String[] ColumnNames = {
			"id",
			"nom",
			"quantite"
	};
	private HashSet<materiels> modified = new HashSet<>();
	
	
	public MyDefaultModel(List<materiels> cities) {
		this.cities = cities;
	}
	
	HashSet<materiels> getModifiedCities(){
		return modified;
	}
	
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 3;
	}

	@Override
	public String getColumnName(int column) {
		// TODO Auto-generated method stub
		return ColumnNames[column];
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return cities == null ? 0 : cities.size();
	}

	@Override
	public Object getValueAt(int row, int column) {
		// TODO Auto-generated method stub
		cities.get(row);
		
		switch (column) {
		case 0:			
			return cities.get(row).getId();
		case 1:
			return cities.get(row).getNom();
		case 2:			
			return cities.get(row).getQuantite();
		default:
			return "";
		}
		

	}

	@Override
	public boolean isCellEditable(int row, int column) {
		// TODO Auto-generated method stub
		if(ColumnNames[column] == "id") {
			return false;
		}
		return true;
		
		
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		// TODO Auto-generated method stub
		Class<?> type = null;
		
		switch (columnIndex) {
		case 0:
			type = Integer.class;
			break;
		case 1:
		case 2:
			type=Integer.class;
			break;
		}
		return type;
	}

	@Override
	public void setValueAt(Object aValue, int row, int column) {
		// TODO Auto-generated method stub
		
		super.setValueAt(aValue, row, column);
		
		materiels c = cities.get(row);
		
		switch (column) {
		case 0:			
			break;
		case 1:
			c.setNom((String) aValue);
			break;
		case 2:	
			c.setQuantite((int) aValue);
			break;
		}
		modified.add(c);
	}
	
	
}
