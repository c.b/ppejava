package model;

import java.io.Serializable;
import javax.persistence.*;



/**
 * The persistent class for the materials database table.
 * 
 */
@Entity
@Table(name="materiels")
@NamedQuery(name="Materiel.findAll", query="SELECT m FROM materiels m")
public class materiels implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int id;

	@Column(name="nom")
	private String nom;

	@Column(name="quantite")
	private int quantite;

	public materiels() {
	}

	public int getId() {
		return this.id;
	}

	public materiels setId(int id) {
		this.id = id;
		return this;
	}
	
	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getQuantite() {
		return this.quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

}