package DAO;


import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import model.User;
import util.HibernateUtil;

public class DAOLogin extends DAOGeneric<User> {

	
	public DAOLogin(Session session) {
		super(session, User.class);
		// TODO Auto-generated constructor stub
	}
	
	
	public User check_user(String email, String password) {
	
		
		String SQL = "SELECT * FROM users WHERE email=:email AND password=:password";
		SQLQuery query = session.createSQLQuery(SQL);
		query.setString("email", email);
		query.setString("password", password);
		query.addEntity(entityClass);
		User user = (User) query.uniqueResult();
		
		return user;
		
	}
	
}
