package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextPane;

public class ShowMedic extends JFrame {

	private JPanel contentPane;
	private JTextField txtNom;
	private JTextField txtQuant;
	private JLabel lblId;
	private JLabel txtID;
	private JButton btnModifier;
	private JButton btnRetour;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ShowMedic frame = new ShowMedic();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ShowMedic() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblId = new JLabel("Id");
		lblId.setBounds(12, 30, 70, 15);
		contentPane.add(lblId);
		
		JLabel lblNomDeProduits = new JLabel("Nom de produits :");
		lblNomDeProduits.setBounds(12, 63, 150, 15);
		contentPane.add(lblNomDeProduits);
		
		txtNom = new JTextField();
		txtNom.setBounds(149, 61, 114, 19);
		contentPane.add(txtNom);
		txtNom.setColumns(10);
		
		JLabel lblQuantiteDispo = new JLabel("Quantite dispo :");
		lblQuantiteDispo.setBounds(12, 102, 138, 15);
		contentPane.add(lblQuantiteDispo);
		
		txtQuant = new JTextField();
		txtQuant.setBounds(133, 100, 114, 19);
		contentPane.add(txtQuant);
		txtQuant.setColumns(10);
		
		txtID = new JLabel("New label");
		txtID.setBounds(67, 30, 70, 15);
		contentPane.add(txtID);
		
		btnModifier = new JButton("Modifier");
		btnModifier.setBounds(20, 147, 117, 25);
		contentPane.add(btnModifier);
		
		btnRetour = new JButton("Retour");
		btnRetour.setBounds(162, 147, 117, 25);
		contentPane.add(btnRetour);
	}
	public JTextField getTxtNom() {
		return txtNom;
	}
	public JTextField getTxtQuant() {
		return txtQuant;
	}
	public JLabel getLblId() {
		return lblId;
	}
	public JLabel getTxtID() {
		return txtID;
	}
	public JButton getBtnModifier() {
		return btnModifier;
	}
	public JButton getBtnRetour() {
		return btnRetour;
	}
}
