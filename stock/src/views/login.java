package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPasswordField;

public class login extends JFrame {

	private JPanel contentPane;
	private JTextField txtid;
	private JTextField txtpass;
	private JButton btnLogin;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					login frame = new login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public login() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 606, 270);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblConnexion = new JLabel("Connexion");
		lblConnexion.setBounds(276, 34, 206, 15);
		contentPane.add(lblConnexion);
		
		JLabel lblEmail = new JLabel("Identifiant");
		lblEmail.setBounds(119, 101, 94, 15);
		contentPane.add(lblEmail);
		
		txtid = new JTextField();
		txtid.setBounds(239, 99, 159, 19);
		contentPane.add(txtid);
		txtid.setColumns(10);
		
		JLabel lblMotDePasse = new JLabel("Mot de passe");
		lblMotDePasse.setBounds(119, 135, 132, 15);
		contentPane.add(lblMotDePasse);
		
		txtpass = new JTextField();
		txtpass.setColumns(10);
		txtpass.setBounds(239, 128, 159, 19);
		contentPane.add(txtpass);
		
		btnLogin = new JButton("Connexion");
		btnLogin.setBounds(239, 162, 159, 25);
		contentPane.add(btnLogin);
	}
	
	
	public JButton getBtnLogin() {
		return btnLogin;
	}
	public JTextField getTxtid() {
		return txtid;
	}
	public JTextField getTxtpass() {
		return txtpass;
	}
}
