package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextField;
import java.awt.Font;

public class stock extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JButton btnUpdate;
	private JTextField textSearch;
	private JButton btnSearch;
	private JButton btnDelete;
	private JButton btnNew;
	private JLabel lblNom;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					stock frame = new stock();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public stock() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 611, 343);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Stock");
		lblNewLabel.setBounds(5, 32, 591, 15);
		contentPane.add(lblNewLabel);
		
		JPanel panel = new JPanel();
		panel.setBounds(5, 269, 591, 35);
		contentPane.add(panel);
		
		JLabel lblCountry = new JLabel("Recherche :");
		panel.add(lblCountry);
		
		textSearch = new JTextField();
		panel.add(textSearch);
		textSearch.setColumns(10);
		
		btnSearch = new JButton("OK");
		panel.add(btnSearch);
		
		btnNew = new JButton("Ajouter");
		btnNew.setEnabled(false);
		panel.add(btnNew);
		
		btnUpdate = new JButton("Modifier");
		btnUpdate.setEnabled(false);
		panel.add(btnUpdate);
		
		btnDelete = new JButton("Supprimer");
		btnDelete.setEnabled(false);
		panel.add(btnDelete);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(5, 59, 591, 249);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
			},
			new String[] {
				"New column", "New column", "New column", "New column"
			}
		));
		scrollPane.setViewportView(table);
		
		JLabel lblBonjour = new JLabel("Bonjour");
		lblBonjour.setFont(new Font("Dialog", Font.BOLD, 12));
		lblBonjour.setBounds(12, 5, 70, 15);
		contentPane.add(lblBonjour);
		
		lblNom = new JLabel("nom");
		lblNom.setBounds(71, 5, 376, 15);
		contentPane.add(lblNom);
	}

	public JTable getJTable() {
		return table;
	}
	public JButton getBtnUpdate() {
		return btnUpdate;
	}
	public JButton getBtnSearch() {
		return btnSearch;
	}
	public JTextField getTextSearch() {
		return textSearch;
	}
	public JButton getBtnDelete() {
		return btnDelete;
	}
	public JButton getBtnNew() {
		return btnNew;
	}
	public JLabel getLblNom() {
		return lblNom;
	}
}
