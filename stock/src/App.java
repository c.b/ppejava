import java.awt.Color;

import Controller.LoginController;
import Controller.StockController;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import DAO.DAOLogin;
import DAO.DAOStock;
import util.HibernateUtil;
import views.login;
import views.stock;

/**
 * 
 */

/**
 * @author student
 *
 */
public class App {


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	
		try {
			
			new LoginController(new login(), new DAOLogin(HibernateUtil.getSessionFactory().openSession()));
			//new StockController(new stock(), new DAOStock(HibernateUtil.getSessionFactory().openSession()));
			
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
